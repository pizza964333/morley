module Michelson.TypeCheck
  ( typeCheckContract
  , typeCheck
  , typeCheckVal
  , typeCheckCVal
  , module M
  ) where

import Michelson.TypeCheck.Value
import Michelson.TypeCheck.Instr
import Michelson.TypeCheck.Types as M

